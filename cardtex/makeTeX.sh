#!/bin/bash

mkdir -p pdfs/
cp alchemy.sty pdfs

while read -r line
do
	name="$(echo "$line" | cut -d"|" -f1)"
	genericMana="$(echo "$line" | cut -d"|" -f4)"
	airMana="$(echo "$line" | cut -d"|" -f5)"
	earthMana="$(echo "$line" | cut -d"|" -f6)"
	fireMana="$(echo "$line" | cut -d"|" -f7)"
	metalMana="$(echo "$line" | cut -d"|" -f8)"
	waterMana="$(echo "$line" | cut -d"|" -f9)"
	woodMana="$(echo "$line" | cut -d"|" -f10)"
	type="$(echo "$line" | cut -d"|" -f12)"
	subtype="$(echo "$line" | cut -d"|" -f13)"
	rarity="$(echo "$line" | cut -d"|" -f14)"
	attack="$(echo "$line" | cut -d"|" -f15)"
	defence="$(echo "$line" | cut -d"|" -f16)"
	resource="$(echo "$line" | cut -d"|" -f17)"
	element="$(echo "$line" | cut -d"|" -f17)"
	keywords="$(echo "$line" | cut -d"|" -f18)"
	ability="$(echo "$line" | cut -d"|" -f20)"
	deathcost="$(echo "$line" | cut -d"|" -f21)"
	deathzone="$(echo "$line" | cut -d"|" -f22)"

	# calculate total cost of the card

	totalCost=$((genericMana + airMana + earthMana + fireMana + metalMana + waterMana + woodMana))
	# determine which colours to use for the cards
	# we start by setting defaults

	primarycolour=white
	secondarycolour=blue
	case $element in
	Air*)
		primarycolour='white'
		secondarycolour='white!50!gray'
		;;
	Earth*)
		primarycolour='brown'
		secondarycolour='brown!50!white'
		;;
	Fire*)
		primarycolour='red'
		secondarycolour='red!50!white'
		;;
	Metal*)
		primarycolour='gray'
		secondarycolour='gray!50!purple'
		;;
	Water*)
		primarycolour='blue'
		secondarycolour='blue!50!white'
		;;
	Wood*)
		primarycolour='green'
		secondarycolour='green!50!white'
		;;
	esac

	# next, time to put together the right tikz node
	# for those fancy mana costs

	tikzNodes=""

	while [[ $"airMana" -gt 0 ]]; do
		airMana=$((airMana-1))
		tikzNodes+="child[concept color=white] { node[concept] {} }\n"
	done

	while [[ $"earthMana" -gt 0 ]]; do
		earthMana=$((earthMana-1))
		tikzNodes+="child[concept color=brown] { node[concept] {} }\n"
	done

	while [[ $"fireMana" -gt 0 ]]; do
		fireMana=$((fireMana-1))
		tikzNodes+="child[concept color=red] { node[concept] {} }\n"
	done

	while [[ $"metalMana" -gt 0 ]]; do
		metalMana=$((metalMana-1))
		tikzNodes+="child[concept color=purple] { node[concept] {} }\n"
	done

	while [[ $"waterMana" -gt 0 ]]; do
		waterMana=$((waterMana-1))
		tikzNodes+="child[concept color=blue] { node[concept] {} }\n"
	done

	while [[ $"woodMana" -gt 0 ]]; do
		woodMana=$((woodMana-1))
		tikzNodes+="child[concept color=green] { node[concept] {} }\n"
	done

	# here we copy the entire TeX template to the 
	# pdfs folder, then substitute the right 
	# variables

	cp template.tex pdfs/"$name".tex

	sed -i "s#TITLE#$name#"          pdfs/"$name".tex
	sed -i "s#RARITY#$rarity#"       pdfs/"$name".tex
	sed -i "s#SUBTYPE#$subtype#"     pdfs/"$name".tex
	sed -i "s#TYPE#$type#"           pdfs/"$name".tex
	sed -i "s#DEATHCOST#$deathcost#"    pdfs/"$name".tex
	sed -i "s#COST#$totalCost#"           pdfs/"$name".tex
	sed -i "s#RESOURCE#$resource#"   pdfs/"$name".tex
	sed -i "s#KEYWORDS#$keywords#"     pdfs/"$name".tex
	sed -i "s#ABILITY#$ability#"     pdfs/"$name".tex
	sed -i "s#ATTACK#$attack#g"      pdfs/"$name".tex
	sed -i "s#DEFENCE#$defence#g"    pdfs/"$name".tex
	sed -i "s#ELEMENT#$element#g"    pdfs/"$name".tex
	sed -i "s#DEATHZONE#$deathzone#g"    pdfs/"$name".tex
	sed -i "s#PRIMARYCOLOUR#$primarycolour#g"    pdfs/"$name".tex
	sed -i "s#SECONDARYCOLOUR#$secondarycolour#g"    pdfs/"$name".tex
	sed -i "s#TIKZNODES#$tikzNodes#"    pdfs/"$name".tex

done < ../output.csv

cd pdfs/
rm Name.tex

for j in *tex
do
	pdflatex "$j"
done

rm *log *aux

