#!/bin/bash

mkdir -p pngs

for i in pdfs/*.pdf
do
	n=$(basename -s pdf "$i")
	convert -density 600 "$i" pngs/"$n"png
done

