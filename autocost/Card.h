#pragma once

#include <iostream>
#include <string>
#include <list>
#include <iterator>

#include "CardCosts.h"
#include "Enums.h"

using namespace std;

class Card
{
private:

	CardCosts costs;

	string cardname;
	int totalcost;
	int x;
	string cost;
	string gen;
	int air;
	int fire;
	int earth;
	int metal;
	int water;
	int wood;
	string type;
	string subtype;
	string rarity;
	string attack;
	string defence;
	char resourcesymbol;
	string resource;
	string keywords;
	int abilitytotalcost;
	string abilitycost;
	string ability;
	string plainability;
	int deathzonetotalcost;
	string deathzonecost;
	string deathzoneability;
	string plaindeathzoneability;
	string flavourtext;

	string additionaltype;
	string additionaltarget;
	string additionalamount;

	string requirementtype;
	string requirementoption;

	string extraeffectcause;
	string extraeffect;
	string extraeffectargs;

	CostToEffect previouscost;
	CostToEffect currentcost;
	string temptext;
	list <string> modifiers;

	int additional = 0;

	bool Keywordfound = false;
	bool Nokeyword = false;

	bool tapped = false;
	bool eot = false;
	bool canattack = false;
	bool other = false;
	bool additionalcost = false;
	bool requirement = false;
	bool extend = false;
	bool targetthis = false;
	bool addextraeffect = false;

	// Functions for dealing with costing and recreating the ability line on cards
	// {
	void Activate(string _condlist);
	void All(string _condlist);
	void Choose(string _condlist);
	void Create(string _condlist);
	void ForEach(string _condlist);
	void Look(string _condlist);
	void Search(string _condlist);
	void Target(string _condlist);
	void Trigger(string _condlist);

	void Ability(string _category, string _target, string _control, string _attack, string _thetarget, string _keyword, string _exception);
	void Add(string _category, string _target, string _control, string _resource, string _amount);
	void Boon(string _category, string _target, string _control, string _attack, string _defence, string _keyword, string _exception);
	void Bounce(string _category, string _target, string _control, string _to,string _amount);
	void CannotAttack(string _category, string _target, string _control);
	void CannotAttackOrBlock(string _category, string _target, string _control);
	void CannotBlock(string _category, string _target, string _control);
	void Capture(string _category, string _target, string _control);
	void CombatDamage(string _category, string _target, string _control, string _thetarget);
	void Copy(string _category, string _target, string _control, string _thetarget);
	void Counter(string _category, string _target, string _control);
	void Damage(string _category, string _target, string _control, string _amount, string _thetarget);
	void Draw(string _category, string _target, string _control, string _amount);
	void Destroy(string _category, string _target, string _control, string _condlist);
	void Discard(string _category, string _target, string _control, string _amount, string _action, string _modifier);
	void Equip(string _category, string _target, string _control, string _cost, string _generic, string _resource);
	void Exhaust(string _category, string _target, string _control, string _amount);
	void ForceAttack(string _category, string _target, string _control);
	void ForceBlock(string _category, string _target, string _control, string _thetarget);
	void Freeze(string _category, string _target, string _control, string _time);
	void Move(string _category, string _target, string _control, string _from, string _to, string _amount, string _thetarget);
	void Rearrange(string _category, string _target, string _control);
	void Reduce(string _category, string _target, string _control, string _what, string _amount);
	void Return(string _category, string _target, string _control, string _to, string _replace);
	void Search(string _category, string _target, string _control, string _thetarget, string _from, string _to);
	void Stand(string _category, string _target, string _control, string _exception);
	void Token(string _category, string _target, string _control, string _amount, string _attack, string _defence, string _name, string _abilities);
	// }

	// Functions for extra ability handling
	// {
	void Ability(string _attack, string _thetarget, string _keyword, string _exception);
	void Add(string _resource, string _amount);
	void Boon(string _attack, string _defence, string _keyword, string _exception);
	void Bounce(string _to, string _amount);
	void CannotAttack();
	void CannotAttackOrBlock();
	void CannotBlock();
	void Capture();
	void CombatDamage(string _thetarget);
	void Copy(string _thetarget);
	void Counter();
	void Damage(string _amount, string _thetarget);
	void Draw(string _amount);
	void Destroy(string _condlist);
	void Discard(string _amount, string _action, string _modifier);
	void Equip(string _cost, string _generic, string _resource);
	void Exhaust(string _amount);
	void ForceAttack();
	void ForceBlock(string _thetarget);
	void Freeze(string _time);
	void Move(string _from, string _to, string _amount, string _thetarget);
	void Rearrange();
	void Reduce(string _what, string _amount);
	void Return(string _to, string _replace);
	void Search(string _thetarget, string _from, string _to);
	void Stand(string _exception);
	void Token(string _amount, string _attack, string _defence, string _name, string _abilities);
	// }

	// Functions to handle modifiers
	void CheckAdditionalCost();
	//

	// Functions to handle the keywords inside ability commands
	// {
	float CheckModifier(string& _condkey);
	float CheckEffect(string& _condkey);
	string GetMandArg(string& _condlist);
	string GetOptArg(string& _condlist);
	string GetArgs(string& _condlist, float _argsize);
	string GetEffect(string& _condlist);
	void CheckFloating(string& _condlist);
	float FindKeyword(string& _condkey);
	float CheckKeyword(string& _condkey);
	void HandleModifers(string& _condlist);
	void HandleExtraEffect();
	void Run(string _condkey, string _modifer, string _modiferlist, string _type, string _target, string _control, string _arglist);
	void RunEffect(string _type, string _target, string _control, string _condkey, string _arglist);
	void RunWithKeyword(string _category, string _target, string _control, string _keyword);
	// }

	// Functions for adding costs to cards
	void CheckTarget(string _target);
	string CheckTarget(string _target, string _type);
	string CheckElement(string _element);
	//

	// Function to set which ability line we are editing
	void SetText(string _text);
public:
	Card();
	~Card();

	Card(Card&&) {};

	Card(const Card&) = delete;
	Card& operator=(const Card&) = delete;

	// Setters for the values of the card
	// {
	void SetName(string _name) { cardname = _name; }
	void SetType(string _type) { type = _type; }
	void SetSubType(string _subtype) { subtype = _subtype; }
	void SetRarity(string _rarity) { rarity = _rarity; }
	void SetAttack(string _attack) { attack = _attack; }
	void SetDefence(string _defence) { defence = _defence; }
	void SetResourceSymbol(char _symbol) { resourcesymbol = _symbol; }
	void SetResource(string _resource) { resource = _resource; }
	void SetKeywords(string _keywords) { keywords = _keywords; }
	void SetAbilityTotalCost(int _totalcost) { abilitytotalcost = _totalcost; }
	void SetAbilityCost(string _cost) { abilitycost = _cost; }
	void SetAbility(string _ability) { ability = _ability; }
	void SetPlainAbility(string _plainability) { plainability = _plainability; }
	void SetDeathzoneTotalCost(int _totalcost) { deathzonetotalcost = _totalcost; }
	void SetDeathzoneCost(string _cost) { deathzonecost = _cost; }
	void SetDeathzoneAbility(string _deathzoneability) { deathzoneability = _deathzoneability; }
	void SetPlainDeathzoneAbility(string _plaindeathzoneability) { plaindeathzoneability = _plaindeathzoneability; }
	void SetFlavourText(string _flavourtext) { flavourtext = _flavourtext; };
	void SetCost(string _cost) { cost = _cost; }
	void SetGeneric(string _generic) { gen = _generic; }
	void SetAir(int _air) { air = _air; }
	void SetEarth(int _earth) { earth = _earth; }
	void SetFire(int _fire) { fire = _fire; }
	void SetMetal(int _metal) { metal = _metal; }
	void SetWater(int _water) { water = _water; }
	void SetWood(int _wood) { wood = _wood; }
	void SetTotalCost(int _cost) { totalcost = _cost; }
	void SetCurrentCost(CostToEffect _cost) { currentcost = _cost; }
	// }

	// Functions for modifying the value of the total cost because of some keywords value
	// {
	void AddToTotalCost(int _cost) { totalcost += _cost; }
	void SubtractTotalCost(int _cost) { totalcost -= _cost; }
	void MultiplyTotalCost(int _cost) { totalcost *= _cost; }

	void AddToAbilityTotalCost(int _cost) { abilitytotalcost += _cost; }
	void SubtractAbilityTotalCost(int _cost) { abilitytotalcost -= _cost; }
	void MultiplyAbilityTotalCost(int _cost) { abilitytotalcost *= _cost; }

	void AddToDeathzoneTotalCost(int _cost) { deathzonetotalcost += _cost; }
	void SubtractDeathzoneTotalCost(int _cost) { deathzonetotalcost -= _cost; }
	void MultiplyDeathzoneTotalCost(int _cost) { deathzonetotalcost *= _cost; }
	// }

	// Functions for handling cost to the correct location.
	// {
	void AddCost(int _value);
	void MultipleCost(int _value);
	void SubtractCost(int _value);
	// }

	// Getters for the values inside of the card
	// {
	string GetName() { return cardname;}
	string GetType() { return type; }
	string GetSubType() { return subtype; }
	string GetRarity() { return rarity; }
	string GetAttack() { return attack; }
	string GetDefence() { return defence; }
	char GetResourceSymbol() { return resourcesymbol; }
	string GetResource() { return resource; }
	string GetKeywords() { return keywords; };
	int GetAbilityTotalCost() { return abilitytotalcost; }
	string GetAbilityCost() { return abilitycost; }
	string GetAbility() { return ability; }
	int GetDeathzoneTotalCost() { return deathzonetotalcost; }
	string GetDeathzoneCost() { return deathzonecost; }
	string GetDeathzoneAbility() { return deathzoneability; }
	string GetCost() { return cost; }
	string GetX() { return to_string(x); }
	string GetGeneric() { return gen; }
	string GetAir() { return to_string(air); }
	string GetEarth() { return to_string(earth); }
	string GetFire() { return to_string(fire); }
	string GetMetal() { return to_string(metal); }
	string GetWater() { return to_string(water); }
	string GetWood() { return to_string(wood); }
	int GetTotalCost() { return totalcost; }
	string GetFlavourText() { return flavourtext; };
	// }

	// Function the prints the card to the console
	void PrintCard();

	// Convert resource to an amount
	void ConvertCost();

	// Functions for handling the keywords
	// {
	void HandleKeyWord(string _name, string _keyword, int _times);
	void HandleKeyWord(string _name, string _keyword, string _arglist);
	// }
};