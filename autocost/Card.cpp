#include "Card.h"

Card::Card()
{
	cardname = "";
	totalcost = 0;
	x = 0;
	cost = "";
	gen = "";
	air = 0;
	earth = 0;
	fire = 0;
	metal = 0;
	water = 0;
	wood = 0;
	type = "";
	subtype = "";
	rarity = "";
	attack = "";
	defence = "";
	keywords = "";
	resourcesymbol = ' ';
	resource = "";
	abilitytotalcost = 0;
	abilitycost = "";
	ability = "";
	deathzonetotalcost = 0;
	deathzonecost = "";
	deathzoneability = "";

	additionaltype = "";
	additionaltarget = "";
	additionalamount = "";

	requirementtype = "";
	requirementoption = "";

	extraeffectcause = "";
	extraeffect = "";
	extraeffectargs = "";

	temptext = "";
}

Card::~Card()
{

}

void Card::PrintCard()
{
	string xcost = "";

	for (int i = 0; i < x; i++)
	{
		xcost += "x";
	}

	cout << "_____________________________" << endl;
	cout << "| " << cardname << "       " << totalcost << endl;
	if (x != 0)
	{
		cout << "| " << type << "       "<< xcost << cost << endl;
	}
	else
	{
		cout << "| " << type << "       " << cost << endl;
	}
	cout << "| " << subtype << endl;
	cout << "| " << rarity << endl;
	if (type == "Unit")
	{
		cout << "| " << attack << endl;
		cout << "| " << defence << endl;
	}
	else
	{
		cout << "| " << endl;
		cout << "| " << endl;
	}
	cout << "| " << resource << endl;
	cout << "| " << keywords << endl;
	cout << "| " <<  endl;
	if (abilitycost == "")
	{
		cout << "| " << ability << endl;
	}
	else
	{
		cout << "| "<< abilitycost << " " << ability << endl;
	}
	cout << "| " << endl;
	cout << "| " << flavourtext << endl;
	cout << "| " << endl;
	if (deathzonecost == "")
	{
		cout << "| " << deathzoneability << endl;
	}
	else
	{
		cout << "| "<< deathzonecost << " " << deathzoneability << endl;
	}
	cout << "_____________________________" << endl;
}

void Card::ConvertCost()
{
	int findgen = -1;

	for (int i = 0; i < cost.length(); i++)
	{
		if (cost[i] == 'A')
		{
			air++;
		}
		else if (cost[i] == 'E')
		{
			earth++;
		}
		else if (cost[i] == 'F')
		{
			fire++;
		}
		else if (cost[i] == 'M')
		{
			metal++;
		}
		else if (cost[i] == 'T')
		{
			water++;
		}
		else if (cost[i] == 'W')
		{
			wood++;
		}
	}

	findgen = cost.find('A');

	for (int i = 0; i < findgen; i++)
	{
		gen += cost[i];
	}

	findgen = cost.find('E');

	for (int i = 0; i < findgen; i++)
	{
		gen += cost[i];
	}

	findgen = cost.find('F');

	for (int i = 0; i < findgen; i++)
	{
		gen += cost[i];
	}

	findgen = cost.find('M');

	for (int i = 0; i < findgen; i++)
	{
		gen += cost[i];
	}

	findgen = cost.find('T');

	for (int i = 0; i < findgen; i++)
	{
		gen += cost[i];
	}

	findgen = cost.find('W');

	for (int i = 0; i < findgen; i++)
	{
		gen += cost[i];
	}
}

// Functions for main keyword statements
// {
void Card::Activate(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string cost = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";
	string gen = "";
	string res = "";
	string amount = "";
	string thetarget = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = GetMandArg(_condlist);
	cost = GetMandArg(_condlist);

	// Handle category related stuff
	if (cost == "exhaust|")
	{
		thetarget = GetMandArg(_condlist);
		target = GetMandArg(_condlist);

		thetarget = thetarget.erase(thetarget.length() - 1);

		AddCost(costs.GetRepeatOnExhaust());

		if (thetarget == "this")
		{
			temptext += "Exhaust, ";
		}
		else
		{
			temptext += "Exhaust ";
			temptext += thetarget;
			temptext += ", ";
		}
	}
	else if (cost == "pay|")
	{
		string tempgen = "";
		string tempres = "";

		gen = GetMandArg(_condlist);
		res = GetOptArg(_condlist);
		target = GetMandArg(_condlist);

		AddCost(costs.GetRepeatOnPay());

		gen = gen.erase(gen.length() - 1);

		if (res != "")
		{
			res = res.substr(1, res.length() - 2);
		}
		else
		{
			res = "0";
		}

		AddToAbilityTotalCost(stoi(gen) * 80);
		AddToAbilityTotalCost(stoi(res) * 100);
	}
	else if (cost == "equip|")
	{
		gen = GetMandArg(_condlist);
		res = GetOptArg(_condlist);
		target = GetMandArg(_condlist);

		gen = gen.erase(gen.length() - 1);

		if (res != "")
		{
			res = res.substr(1, res.length() - 2);
		}
		else
		{
			res = "0";
		}

		temptext += "equip, ";

		AddToAbilityTotalCost(stoi(gen) * 80);
		AddToAbilityTotalCost(stoi(res) * 100);
	}
	else if (cost == "expay|")
	{
		gen = GetMandArg(_condlist);
		res = GetOptArg(_condlist);
		target = GetMandArg(_condlist);

		gen = gen.erase(gen.length() - 1);

		if (res != "")
		{
			res = res.substr(1, res.length() - 2);
		}
		else
		{
			res = "0";
		}

		AddToAbilityTotalCost(stoi(gen) * 80);
		AddToAbilityTotalCost(stoi(res) * 100);

		if (target == "this|")
		{
			temptext += "Exhaust, ";
		}
		else
		{
			temptext += "Exhaust ";
			temptext += target;
			temptext += ", ";
		}
	}
	else if (cost == "sac|")
	{
		amount = GetMandArg(_condlist);
		thetarget = GetMandArg(_condlist);
		target = GetMandArg(_condlist);

		amount = amount.erase(amount.length() - 1);
		thetarget = thetarget.erase(thetarget.length() - 1);

		AddCost(costs.GetRepeatOnSac());

		temptext += "Sacrafice ";

		if (thetarget == "perm")
		{
			thetarget = "permanent";
		}

		if (thetarget == "this")
		{
			temptext += GetName();
		}
		else
		{
			if (amount == "1")
			{
				temptext += thetarget;
			}
			else
			{
				temptext += amount;
				temptext += " ";
				temptext += thetarget;
				temptext += "s";
			}
		}

		temptext += ", ";
	}

	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::All(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = "all|";
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::Choose(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = "choose|";
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::Create(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = GetMandArg(_condlist);
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::ForEach(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = "for each|";
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::Look(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = GetMandArg(_condlist);
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::Search(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = GetMandArg(_condlist);
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::Target(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = "target|";
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, effect);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

void Card::Trigger(string _condlist)
{
	// Set variables to hold the command line
	string category = "";
	string target = "";
	string control = "";
	string modifier = "";
	string modifierarglist = "";
	string effect = "";
	string effectargslist = "";
	string temp = "";
	string arg = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	float commandargs = 0.0f;
	float effectargs = 0.0f;
	float modifierargs = 0.0f;

	// Setting the function defaults
	temptext = "";
	additional = 0;
	previouscost = currentcost;

	// Grab the madatory arguments
	category = GetMandArg(_condlist);
	target = GetMandArg(_condlist);
	control = GetMandArg(_condlist);

	// Check for modifiers
	HandleModifers(_condlist);

	// Get the effects for the command
	effect = GetEffect(_condlist);

	// Check the condkey for args
	effectargs = CheckEffect(effect);

	if (Keywordfound == true)
	{
		string keyword = "";

		RunWithKeyword(category, target, control, keyword);
		Keywordfound = false;
	}
	else
	{
		effectargslist = GetArgs(_condlist, effectargs);

		Run(effect, modifier, modifierarglist, category, target, control, effectargslist);
	}
}

// Functions to handle modifiers
void Card::CheckAdditionalCost()
{
	if (additionaltype == "exhaust")
	{
		temptext += "exhaust ";

		if (additionalamount == "1")
		{
			temptext += additionaltarget;
			temptext += ", ";
		}
		else
		{
			temptext += additionalamount;
			temptext += " ";
			temptext += additionaltarget;
			temptext += ", ";
		}
	}
}
//

void Card::Add(string _category, string _target, string _control, string _resource, string _amount)
{
	string category = "";
	string target = "";
	string control = "";
	string resource = "";
	string amount = "";

	category = _category;
	target = _target;
	control = _control;
	resource = _resource.erase(_resource.length() - 1);
	amount = _amount.erase(_amount.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "activate")
	{
		resource = CheckElement(resource);

		temptext += "add ";
		temptext += amount;
		temptext += " ";
		temptext += resource;
		temptext += " to your resource stock ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{

	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
}

void Card::Ability(string _category, string _target, string _control, string _thetarget, string _amount, string _keyword, string _exception)
{
	string category = "";
	string target = "";
	string control = "";
	string amount = "";
	string thetarget = "";
	string keyword = "";
	string exc = "";

	category = _category;
	target = _target;
	control = _control;
	amount = _amount.erase(_amount.length() - 1);
	thetarget = _thetarget.erase(_thetarget.length() - 1);
	keyword = _keyword.erase(_keyword.length() - 1);

	if (_exception != "")
	{
		exc = _exception.substr(1, _exception.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (amount == "all")
	{
		temptext += "all ";
	}

	if (exc == "this")
	{
		temptext += "other ";
	}

	temptext += thetarget;
	temptext += " units gain ";
	temptext += keyword;
}

void Card::Boon(string _category, string _target, string _control, string _attack, string _defence, string _keyword, string _exception)
{
	string category = "";
	string target = "";
	string control = "";
	string attack = "";
	string defence = "";
	string keyword = "";
	string exc = "";

	int att = 0;
	int def = 0;

	category = _category;
	target = _target;
	control = _control;
	attack = _attack.erase(_attack.length() - 1);
	defence = _defence.erase(_defence.length() - 1);

	if (_keyword != "")
	{
		keyword = _keyword.substr(1, _keyword.length() - 2);
	}

	if (_exception != "")
	{
		exc = _exception.substr(1, _exception.length() - 2);
	}

	att = stoi(attack);
	def = stoi(defence);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (targetthis == true)
	{
		temptext += target;
		temptext += " ";
	}
	else
	{
		if (category == "all")
		{
			temptext += "all ";
			temptext += target;
			temptext += "s ";
		}
		else if (category == "target")
		{
			AddCost(costs.GetTargetUnit());
			if (type == "Enhancement")
			{
				temptext += "enhanced ";
				temptext += target;
				temptext += " ";
			}
			else
			{
				temptext += "target ";
				temptext += target;
				temptext += " ";
			}

			if (control == "you")
			{
				temptext += "you control ";
			}
		}
		else if (category == "when")
		{
			temptext += "when ";
		}
		else if (category == "start")
		{
			temptext += "at the start of your turn ";
		}
	}

	if (exc == "other")
	{
		temptext += "other ";
		temptext += target;
		temptext += " ";
	}

	temptext += "gains ";
	AddCost(costs.GetBoonAttack() * att);
	temptext += attack;
	AddCost(costs.GetBoonDefence() * def);
	temptext += "/";
	temptext += defence;
	temptext += " ";

	if (keyword != "")
	{
		temptext += "and ";
		AddCost(costs.CheckKeyword(keyword));
		temptext += keyword;
		temptext += " ";
	}

	if (exc != "")
	{
		temptext += "except ";
		temptext += exc;
		temptext += " ";
	}

	if (type == "Trick" || eot == true)
	{
		temptext += "until end of turn. ";
	}
}

void Card::Bounce(string _category, string _target, string _control, string _to, string _amount)
{
	string category = "";
	string target = "";
	string control = "";
	string to = "";
	string amount = "";

	category = _category;
	target = _target;
	control = _control;
	to = _to.erase(_to.length() - 1);
	amount = _amount.erase(_amount.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		CheckTarget(target);

		if (amount == "1")
		{
			temptext += "target ";
			temptext += target;
			temptext += " ";
		}
		else
		{
			temptext += "up to ";
			temptext += amount;
			temptext += " target ";
			temptext += target;
			temptext += "s ";
		}
		
		if (control == "you")
		{
			temptext += "you control ";
		}

		temptext += "is returned to its owners ";
		temptext += to;
		temptext == " ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
}

void Card::CannotAttack(string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category;
	target = _target;
	control = _control;

	AddCost(costs.GetPreventAttack());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		temptext += "target ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += target;

	if (category == "all")
	{
		temptext += "s";
	}

	if (control == "opoonent")
	{
		temptext += " target opponent control";
	}
	else if (control == "opoonents")
	{
		temptext += " your opponents control";
	}

	temptext += " cannot attack ";
}

void Card::CannotAttackOrBlock(string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category;
	target = _target;
	control = _control;

	AddCost(costs.GetPreventBoth());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		if (type == "Enhancement")
		{
			temptext += "enhanced ";
		}
		else
		{
			temptext += "target ";
		}
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += target;

	if (category == "all")
	{
		temptext += "s";
	}

	if (control == "opoonent")
	{
		temptext += " target opponent control";
	}
	else if (control == "opoonents")
	{
		temptext += " your opponents control";
	}

	temptext += " cannot attack or block ";
}

void Card::CannotBlock( string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category;
	target = _target;
	control = _control;

	AddCost(costs.GetPreventBlock());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		temptext += "target ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += target;

	if (category == "all")
	{
		temptext += "s";
	}

	if (control == "opoonent")
	{
		temptext += " target opponent control";
	}
	else if (control == "opoonents")
	{
		temptext += " your opponents control";
	}

	temptext += " cannot block ";
}

void Card::Capture(string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";
	string thetarget = "";

	category = _category;
	target = _target;
	control = _control;

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{

	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
}

void Card::CombatDamage(string _category, string _target, string _control, string _thetarget)
{
	string category = "";
	string target = "";
	string control = "";
	string thetarget = "";

	category = _category;
	target = _target;
	control = _control;
	thetarget = _thetarget.erase(_thetarget.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{

	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (target == "this")
	{
		temptext += GetName();
		temptext += " ";
	}
	else
	{
		temptext += "a ";
		temptext += target;
		temptext += " ";
	}

	temptext += "deals combat damage to a ";
	temptext += thetarget;
	temptext += " ";
}

void Card::Copy(string _category, string _target, string _control, string _thetarget)
{
	string category = "";
	string target = "";
	string control = "";
	string thetarget = "";

	category = _category;
	target = _target;
	control = _control;

	if (_thetarget != "")
	{
		_thetarget = _thetarget.substr(1, _thetarget.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}
}

void Card::Counter(string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";
	string thetarget = "";

	category = _category;
	target = _target;
	control = _control;

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		CheckTarget(target);

		temptext += "counter target ";

		if (target == "any")
		{
			target = CheckTarget(target, "counter");
		}

		temptext += target;
		temptext += " ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
}

void Card::Damage(string _category, string _target, string _control, string _amount, string _thetarget)
{
	string category = "";
	string target = "";
	string control = "";
	string amount = "";
	string thetarget = "";

	category = _category;
	target = _target;
	control = _control;
	amount = _amount.erase(_amount.length() - 1);

	if (_thetarget != "")
	{
		thetarget = _thetarget.substr(1, _thetarget.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "target")
	{
		temptext += "target ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (amount == "x")
	{
		x++;
	}
	else
	{
		if (target == "unit")
		{
			for (int i = 0; i < stoi(amount); i++)
			{
				AddCost(costs.GetDamageToUnit());
			}

			temptext += target;
			temptext += " ";
		}
		else if (target == "permanent")
		{
			for (int i = 0; i < stoi(amount); i++)
			{
				AddCost(costs.GetDamageToAny());
			}

			temptext += "permanent ";
		}
		else if (target == "player")
		{
			for (int i = 0; i < stoi(amount); i++)
			{
				AddCost(costs.GetDamageToPlayer());
			}

			temptext += target;
			temptext += " ";
		}
		else if (target == "you")
		{
			for (int i = 0; i < stoi(amount); i++)
			{
				AddCost(costs.GetDamageToPlayer());
			}

			temptext += target;
			temptext += " ";
		}
		else if (target == "any")
		{
			for (int i = 0; i < stoi(amount); i++)
			{
				AddCost(costs.GetDamageToAny());
			}

			temptext += "unit or player ";
		}

		if (requirement == true)
		{
			if (requirementtype == "with")
			{
				temptext += "with ";
				temptext += requirementoption;
				temptext += " ";
			}
		}

		temptext += "takes ";
		temptext += amount;
		temptext += " damage ";
	}
}

void Card::Destroy(string _category, string _target, string _control, string _requirement)
{
	string category = "";
	string target = "";
	string control = "";
	string requirment = "";

	category = _category;
	target = _target;
	control = _control;

	if (_requirement != "")
	{
		requirment = _requirement.substr(1, _requirement.length() - 2);
	}

	AddCost(costs.GetDestroy());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "target")
	{
		temptext += "target ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (requirment != "")
	{
		temptext += requirment;
		temptext += " ";
	}

	temptext += target;

	if (category == "all")
	{
		if (control == "player")
		{
			temptext += "s target player controls are destroyed ";
		}
		else if(control == "opponents")
		{
			temptext += "s you don't control are destroyed ";
		}
		else if (control == "all")
		{
			temptext += "s are destroyed ";
		}
	}
	else if (category == "target")
	{
		if (control == "player")
		{
			temptext += " target player controls are destroyed ";
		}
		else if (control == "opponent")
		{
			temptext += " you don't control are destroyed ";
		}
		else if (control == "any")
		{
			temptext += " is destroyed ";
		}
	}

	if (requirment == "deathzone")
	{
		temptext += "and goes to deathzone instead of the graveyard ";
	}
}

void Card::Discard(string _category, string _target, string _control, string _amount, string _action, string _modifier)
{
	string category = "";
	string target = "";
	string control = "";
	string amount = "";
	string action = "";
	string mod = "";

	category = _category;
	target = _target;
	control = _control;
	amount = _amount.erase(_amount.length() - 1);
	action = _action.erase(_action.length() - 1);

	if (_modifier != "")
	{
		mod = _modifier.substr(1, _modifier.length() - 2);
	}

	if (category == "target")
	{
		if (control == "you")
		{
			temptext += "discard ";
		}
	}

	if (amount == "hand")
	{
		temptext += "your hand ";
	}

	temptext += "and ";
	temptext += action;
	temptext += " that many ";
	temptext += mod;
	temptext == " ";
}

void Card::Draw(string _category, string _target, string _control, string _amount)
{
	string category = "";
	string target = "";
	string control = "";
	string amount = "";

	category = _category;
	target = _target;
	control = _control;
	amount = _amount.erase(_amount.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (target == "player")
	{
		AddCost(costs.GetTargetPlayer());
		temptext += "target player ";
	}
	else if (target == "you")
	{
		AddCost(costs.GetTargetYou());
		temptext += "you ";
	}

	temptext += "draw ";

	if (amount == "x")
	{
		x++;
		temptext += "x ";
	}
	else
	{
		for (int i = 0; i < stoi(amount); i++)
		{
			AddCost(costs.GetDrawCard());
		}
		temptext += amount;
		temptext += " ";
	}
}

void Card::Equip(string _category, string _target, string _control, string _cost, string _generic, string _resource)
{
	string category = "";
	string target = "";
	string control = "";
	string cost = "";
	string gen = "";
	string res = "";

	category = _category;
	target = _target;
	control = _control;
	cost = _cost.erase(_cost.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += "enchance target ";
	temptext += target;
	temptext += " gain ";
}

void Card::Exhaust(string _category, string _target, string _control, string _amount)
{
	string category = "";
	string target = "";
	string control = "";
	string amount = "";

	category = _category;
	target = _target;
	control = _control;
	amount = _amount.erase(_amount.length() - 1);

	CheckTarget(target);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	for (int i = 0; i < stoi(amount); i++)
	{
		AddCost(costs.GetExhaust());
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		temptext += "exhaust ";

		if (amount == "1")
		{
			temptext += "target ";
		}
		else
		{
			temptext += "upto ";
			temptext += amount;
			temptext += " target ";
		}
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += target;

	if (category == "all")
	{
		temptext += "s are exhausted ";
	}
}

void Card::ForceAttack(string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category;
	target = _target;
	control = _control;

	AddCost(costs.GetForceAttack());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		temptext += "taregt ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += target;
	temptext += " must attack if able ";
}

void Card::ForceBlock(string _category, string _target, string _control, string _thetarget)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category;
	target = _target;
	control = _control;

	AddCost(costs.GetForceBlock());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all ";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		if (type == "Enhancement")
		{
			temptext += "enhanced you gains all ";
			temptext += target;
			temptext += "s able to block must block ";
		}
		else
		{
			temptext += "target ";
		}
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
}

void Card::Freeze(string _category, string _target, string _control, string _time)
{
	string category = "";
	string target = "";
	string control = "";
	string time = "";

	category = _category;
	target = _target;
	control = _control;
	time = _time.erase(_time.length() - 1);

	AddCost(costs.GetFreeze());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "all";
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		temptext += "target ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	temptext += target;
	temptext += " doesn't ready on it's controllers next ";

	if (time == "1")
	{
		temptext += "turn ";
	}
	else
	{
		temptext += time;
		temptext += " turns ";
	}
}

void Card::Move(string _category, string _target, string _control, string _from, string _to, string _amount, string _thetarget)
{
	string category = "";
	string target = "";
	string control = "";
	string from = "";
	string to = "";
	string amount = "";
	string thetarget = "";

	category = _category;
	target = _target;
	control = _control;
	from = _from.erase(_from.length() - 1);
	to = _to.erase(_to.length() - 1);
	amount = _amount.erase(_amount.length() - 1);

	if (_thetarget != "")
	{
		thetarget = _thetarget.substr(1, _thetarget.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "target")
	{
		temptext += "target ";
		temptext += target;
		temptext += " moves from ";
		temptext += from;
		temptext += " to ";
		temptext += to;
	}
	else if (category == "when")
	{
		temptext += "when ";

	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
	else if (category == "enter")
	{
		temptext += "when ";
		temptext += target;
		temptext += " enters the battlefield, return all ";
		temptext += thetarget;
		temptext += " from ";
		temptext += from;
		temptext += " to ";
		temptext += to;
		temptext += " ";
	}

	
}

void Card::Rearrange(string _category, string _target, string _control)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category;
	target = _target;
	control = _control;

	AddCost(costs.GetRearrange());

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{

	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}
}

void Card::Reduce(string _category, string _target, string _control, string _what, string _amount)
{
	string category = "";
	string target = "";
	string control = "";
	string what = "";
	string amount = "";

	category = _category;
	target = _target;
	control = _control;
	what = _what.erase(_what.length() - 1);
	amount = _amount.erase(_amount.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{

	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	for (int i = 0; i < stoi(amount); i++)
	{
		AddCost(costs.GetReduceGeneric());
	}

	temptext += "reduce the cost of target ";
	temptext += what;
	temptext += " by ";
	temptext += amount;
	temptext += " ";
}

void Card::Return(string _category, string _target, string _control, string _to, string _replace)
{
	string category = "";
	string target = "";
	string control = "";
	string to = "";
	string replace = "";

	category = _category;
	target = _target;
	control = _control;
	to = _to.erase(_to.length() - 1);

	if (_replace != "")
	{
		replace = _replace.substr(1, _replace.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{
		temptext += "return all ";
		temptext += target;

		if (control == "you")
		{
			temptext += " you control to their owners hands. ";
		}
		else if (control == "opponents")
		{
			temptext += " you don't control to their owners hands. ";
		}
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{

	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (replace == "upto")
	{
		temptext += "you may put up to that many ";

		if (target == "resource")
		{
			temptext += "cards from your hand as resource cards into play ";
		}
	}

	if (tapped == true)
	{
		temptext += "exhausted ";
	}
}

void Card::Search(string _category, string _target, string _control, string _thetarget, string _from, string _to)
{
	string category = "";
	string target = "";
	string control = "";
	string thetarget = "";
	string from = "";
	string to = "";

	category = _category;
	target = _target;
	control = _control;
	thetarget = _thetarget.erase(_thetarget.length() - 1);
	from = _from.erase(_from.length() - 1);
	to = _to.erase(_to.length() - 1);

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "choose")
	{
		temptext += "choose ";

		if (thetarget == "element")
		{
			temptext += "an ";
		}

		temptext += thetarget;
		temptext += ". ";
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	AddCost(costs.GetSearch());
	temptext += "search ";
	
	if (control == "you")
	{
		temptext += "your ";
	}

	if (from == "any")
	{
		temptext += "chronicle, graveyard or deathzone for ";
	}
	else if (from == "chronicle")
	{
		AddCost(costs.GetFromChronicle());
		temptext += "chronicle for a ";
	}
	else if (from == "archieve")
	{
		AddCost(costs.GetFromArchieve());
		temptext += "archieve for a ";
	}
	

	if (thetarget == "element")
	{
		temptext += "card in that element ";
	}
	else
	{
		temptext += thetarget;
		temptext += " ";
	}

	temptext += "and put it ";

	if (to == "hand")
	{
		AddCost(costs.GetToHand());
		temptext += "into your hand. ";
	}
	else if (to == "grave")
	{
		AddCost(costs.GetToGrave());
		temptext += "into your graveyard. ";
	}
	else if (to == "chronicle")
	{
		AddCost(costs.GetToChronicle());
		temptext += "into your chronicle and shuffle it. ";
	}
	else if (to == "battlefield")
	{
		AddCost(costs.GetToBattlefield());
		temptext += "into the battlefield. ";
	}
}

void Card::Stand(string _category, string _target, string _control, string _exception)
{
	string category = "";
	string target = "";
	string control = "";
	string exc = "";

	category = _category;
	target = _target;
	control = _control;

	if (_exception != "")
	{
		exc = _exception.substr(1, _exception.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "all")
	{

	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		
	}
	else if (category == "when")
	{
		temptext += "when ";
	}
	else if (category == "start")
	{
		temptext += "at the start of your turn ";
	}

	if (exc == "other")
	{
		AddCost(costs.GetStand());
		temptext += "stand another ";
	}
	else
	{
		AddCost(costs.GetStand());
		temptext += "stand ";
	}

	if (target == "this")
	{
		temptext += GetName();
		temptext += " ";
	}
	else if (target == "unit")
	{
		AddCost(costs.GetTargetUnit());
		temptext += "target ";
		temptext += target;
		temptext += " ";
	}
	else if (target == "enhancement")
	{
		AddCost(costs.GetTargetEnhancement());
		temptext += "target ";
		temptext += target;
		temptext += " ";
	}
	else if (target == "resource")
	{
		AddCost(costs.GetTargetResource());
		temptext += "target ";
		temptext += target;
		temptext += " ";
	}
	else if (target == "permanent")
	{
		AddCost(costs.GetTargetPerm());
		temptext += "target ";
		temptext += target;
		temptext += " ";
	}

	if (canattack = true)
	{
		temptext += "it may attack again this turn ";
	}
}

void Card::Token(string _category, string _target, string _control, string _amount, string _attack, string _defence, string _name, string _abilities)
{
	string category = "";
	string target = "";
	string control = "";
	string amount = "";
	string attack = "";
	string defence = "";
	string name = "";
	string abilities = "";

	category = _category;
	target = _target;
	control = _control;
	amount = _amount.erase(_amount.length() - 1);
	attack = _attack.erase(_attack.length() - 1);
	defence = _defence.erase(_defence.length() - 1);

	if (_name != "")
	{
		name = _name.substr(1, _name.length() - 2);
	}

	if (_abilities != "")
	{
		abilities = _abilities.substr(1, _abilities.length() - 2);
	}

	if (additionalcost == true)
	{
		CheckAdditionalCost();
	}

	if (category == "token")
	{
		temptext += "create ";

		if (amount == "1")
		{
			temptext += "a token ";
			temptext += target;
			temptext += " ";
		}
		else
		{
			temptext += amount;
			temptext += " token ";
			temptext += target;
			temptext += "s ";
		}

		if (name != "")
		{
			temptext += "called ";
			temptext += name;
			temptext += " ";
		}
		temptext += "with ";
		AddCost(costs.GetTokenAttack() * stoi(attack));
		temptext += attack;
		temptext += "/";
		AddCost(costs.GetTokenDefence() * stoi(defence));
		temptext += defence;
		temptext += " ";

		if (abilities != "")
		{
			AddCost(costs.CheckKeyword(abilities));
			temptext += "and ";
			temptext += abilities;
			temptext += " ";
		}
	}
	else if (category == "for each")
	{
		temptext += "for each ";
		temptext += target;
		temptext += " target ";
		temptext += control;
		temptext += " controls ";

		temptext += "create ";

		if (amount == "1")
		{
			temptext += "a token ";
			temptext += target;
			temptext += " ";
		}
		else
		{
			temptext += amount;
			temptext += " token ";
			temptext += target;
			temptext += "s ";
		}

		if (name != "")
		{
			temptext += "called ";
			temptext += name;
			temptext += " ";
		}
		temptext += "with ";
		AddCost(costs.GetTokenAttack() * stoi(attack));
		temptext += attack;
		temptext += "/";
		AddCost(costs.GetTokenDefence() * stoi(defence));
		temptext += defence;
		temptext += " ";

		if (abilities != "")
		{
			AddCost(costs.CheckKeyword(abilities));
			temptext += "and ";
			temptext += abilities;
			temptext += " ";
		}
	}
	else if (category == "target")
	{

	}
	else if (category == "counter")
	{
		temptext += "target ";
		temptext += target;
		temptext += " gets ";
		temptext += amount;
		temptext += " ";
		temptext += attack;
		temptext += "/";
		temptext += defence;
		temptext == " counter ";
	}
}
// }

// Functions for extra ability handling
// {
void Card::Ability(string _attack, string _thetarget, string _keyword, string _exception)
{

}

void Card::Add(string _resource, string _amount)
{

}

void Card::Boon(string _attack, string _defence, string _keyword, string _exception)
{

}

void Card::Bounce(string _to, string _amount)
{

}

void Card::CannotAttack()
{

}

void Card::CannotAttackOrBlock()
{

}

void Card::CannotBlock()
{

}

void Card::Capture()
{

}

void Card::CombatDamage(string _thetarget)
{

}

void Card::Copy(string _thetarget)
{

}

void Card::Counter()
{

}

void Card::Damage(string _amount, string _thetarget)
{
	string amount = "";
	string thetarget = "";

	amount = _amount.erase(_amount.length() - 1);

	if (_thetarget != "")
	{
		thetarget = _thetarget.substr(1, _thetarget.length() - 2);
	}

	if (extraeffectcause == ",dies.")
	{
		temptext += "if it dies, deal ";
		temptext += amount;
		temptext += " damage to target ";
		temptext += thetarget;
		temptext += " ";
	}
}

void Card::Draw(string _amount)
{

}

void Card::Destroy(string _condlist)
{

}

void Card::Discard(string _amount, string _action, string _modifier)
{
	string amount = "";
	string action = "";
	string mod = "";

	amount = _amount.erase(_amount.length() - 1);
	action = _action.erase(_action.length() - 1);

	if (_modifier != "")
	{
		mod = _modifier.substr(1, _modifier.length() - 2);
	}
}

void Card::Equip(string _cost, string _generic, string _resource)
{

}

void Card::Exhaust(string _amount)
{

}

void Card::ForceAttack()
{

}

void Card::ForceBlock(string _thetarget)
{

}

void Card::Freeze(string _time)
{

}

void Card::Move(string _from, string _to, string _amount, string _thetarget)
{

}

void Card::Rearrange()
{

}

void Card::Reduce(string _what, string _amount)
{

}

void Card::Return(string _to, string _replace)
{

}

void Card::Search(string _thetarget, string _from, string _to)
{

}

void Card::Stand(string _exception)
{

}

void Card::Token(string _amount, string _attack, string _defence, string _name, string _abilities)
{

}
// }

// Functions for adding costs to the cards
void Card::CheckTarget(string _target)
{
	if (_target == "unit")
	{
		AddCost(costs.GetTargetUnit());
	}
	else if (_target == "enhancement")
	{
		AddCost(costs.GetTargetEnhancement());
	}
	else if (_target == "resource")
	{
		AddCost(costs.GetTargetResource());
	}
	else if (_target == "permanent")
	{
		AddCost(costs.GetTargetPerm());
	}
	else if (_target == "opponent")
	{
		AddCost(costs.GetTargetOpponent());
	}
	else if (_target == "everyone")
	{
		AddCost(costs.GetTargetEveryone());
	}
	else if (_target == "player")
	{
		AddCost(costs.GetTargetPlayer());
	}
	else if (_target == "you")
	{
		AddCost(costs.GetTargetYou());
	}
}

string Card::CheckTarget(string _target, string _type)
{
	if (_type == "counter")
	{
		if (_target == "any")
		{
			return "spell or ability";
		}
	}
}

string Card::CheckElement(string _element)
{
	if (_element == "air")
	{
		return "A";
	}
	else if (_element == "any")
	{
		return "any resource";
	}
	else if (_element == "earth")
	{
		return "e";
	}
	else if (_element == "fire")
	{
		return "f";
	}
	else if (_element == "metal")
	{
		return "m";
	}
	else if (_element == "water")
	{
		return "w";
	}
	else if (_element == "wood")
	{
		return "w";
	}

	return "not found";
}
//

// Functions for handling cost to the correct location.
// {
void Card::AddCost(int _value)
{
	if (currentcost == CostToEffect::cardcost)
	{
		AddToTotalCost(_value);
	}
	if (currentcost == CostToEffect::deathzonecost)
	{
		AddToDeathzoneTotalCost(_value);
	}
}

void Card::MultipleCost(int _value)
{
	if (currentcost == CostToEffect::cardcost)
	{
		MultiplyTotalCost(_value);
	}
	if (currentcost == CostToEffect::deathzonecost)
	{
		MultiplyDeathzoneTotalCost(_value);
	}
}

void Card::SubtractCost(int _value)
{
	if (currentcost == CostToEffect::cardcost)
	{
		SubtractTotalCost(_value);
	}
	if (currentcost == CostToEffect::deathzonecost)
	{
		SubtractDeathzoneTotalCost(_value);
	}
}
// }

// Functions to handle the keywords inside ability commands
// {
float Card::CheckModifier(string& _condkey)
{
	int key = -1;
	int foundtarget = -1;

	float argsize = 0.0f;

	if (_condkey == ",additional.")
	{
		key = as_int(Modifiers::additional);
	}
	else if (_condkey == ",attack.")
	{
		key = as_int(Modifiers::attack);
	}
	else if (_condkey == ",combat damage.")
	{
		key = as_int(Modifiers::combatdamage);
	}
	else if (_condkey == ",dies.")
	{
		key = as_int(Modifiers::dies);
	}
	else if (_condkey == ",enter.")
	{
		key = as_int(Modifiers::enter);
	}
	else if (_condkey == ",extend.")
	{
		key = as_int(Modifiers::extend);
	}
	else if (_condkey == ",may attack.")
	{
		key = as_int(Modifiers::mayattack);
	}
	else if (_condkey == ",other.")
	{
		key = as_int(Modifiers::other);
	}
	else if (_condkey == ",requirement.")
	{
		key = as_int(Modifiers::requirement);
	}
	else if (_condkey == ",exhausted.")
	{
		key = as_int(Modifiers::tapped);
	}

	switch (key)
	{
	// Additional
	case 0:
		return 3.0f;
	// Attack
	case 1:
		return 0.0f;
	// Combat damage
	case 2:
		return 1.0f;
	// Dies
	case 3:
		return 10.0f;
	// Enter
	case 4:
		return 1.0f;
	// Extend
	case 5:
		return 1.1f;
	// May attack
	case 6:
		return 0.0f;
	// Other
	case 7:
		return 0.0f;
	// Requirement
	case 8:
		return 1.1f;
	// Exhausted
	case 9:
		return 0.0f;
	case -1:
		cout << "Modifier Handler: No modifier key found.  " << _condkey << endl;
		break;
	}

	return argsize;
}

float Card::CheckEffect(string& _condkey)
{
	int key = -1;
	float keyfloat = 0.0f;

	if (_condkey == "ability")
	{
	key = as_int(Effects::ability);
	}
	else if (_condkey == "add")
	{
		key = as_int(Effects::add);
	}
	else if (_condkey == "boon")
	{
		key = as_int(Effects::boon);
	}
	else if (_condkey == "bounce")
	{
		key = as_int(Effects::bounce);
	}
	else if (_condkey == "cannotattack")
	{
		key = as_int(Effects::cannotattack);
	}
	else if (_condkey == "cannotattackorblock")
	{
		key = as_int(Effects::cannotattackorblock);
	}
	else if (_condkey == "cannotblock")
	{
		key = as_int(Effects::cannotblock);
	}
	else if (_condkey == "capture")
	{
		key = as_int(Effects::capture);
	}
	else if (_condkey == "combatdamage")
	{
		key = as_int(Effects::combatdamage);
	}
	else if (_condkey == "copy")
	{
		key = as_int(Effects::copy);
	}
	else if (_condkey == "counter")
	{
		key = as_int(Effects::counter);
	}
	else if (_condkey == "damage")
	{
		key = as_int(Effects::damage);
	}
	else if (_condkey == "destroy")
	{
		key = as_int(Effects::destroy);
	}
	else if (_condkey == "discard")
	{
		key = as_int(Effects::discard);
	}
	else if (_condkey == "draw")
	{
		key = as_int(Effects::draw);
	}
	else if (_condkey == "equip")
	{
		key = as_int(Effects::equip);
	}
	else if (_condkey == "exhaust")
	{
		key = as_int(Effects::exhaust);
	}
	else if (_condkey == "extend")
	{
		key = as_int(Effects::extend);
	}
	else if (_condkey == "forceattack")
	{
		key = as_int(Effects::forceattack);
	}
	else if (_condkey == "forceblock")
	{
		key = as_int(Effects::forceblock);
	}
	else if (_condkey == "freeze")
	{
		key = as_int(Effects::freeze);
	}
	else if (_condkey == "move")
	{
		key = as_int(Effects::move);
	}
	else if (_condkey == "multiblock")
	{
		key = as_int(Effects::multiblock);
	}
	else if (_condkey == "rearrange")
	{
		key = as_int(Effects::rearrange);
	}
	else if (_condkey == "reduce")
	{
		key = as_int(Effects::reduce);
	}
	else if (_condkey == "return")
	{
		key = as_int(Effects::retrn);
	}
	else if (_condkey == "search")
	{
		key = as_int(Effects::search);
	}
	else if (_condkey == "stand")
	{
		key = as_int(Effects::stand);
	}
	else if (_condkey == "swap")
	{
	key = as_int(Effects::swap);
	}
	else if (_condkey == "token")
	{
		key = as_int(Effects::token);
	}

	switch (key)
	{
	// Ability
	case 0:
		return 3.1;
		break;
	// Add
	case 1:
		return 2.0f;
		break;
	// Boon
	case 2:
		return 2.2f;
		break;
	// Bounce
	case 3:
		return 2.2f;
		break;
	// Cannot attack
	case 4:
		return 0.0f;
		break;
	// Cannot attack or block
	case 5:
		return 0.0f;
		break;
	// Cannot block
	case 6:
		return 0.0f;
		break;
	// Capture
	case 7:
		return 1.1f;
		break;
	// Combat damage
	case 8:
		return 0.0f;
		break;
	// Copy
	case 9:
		return 0.0f;
		break;
	// Counter
	case 10:
		return 0.0f;
		break;
	// Damage
	case 11:
		return 1.1f;
		break;
	// Destroy
	case 12:
		return 0.1f;
		break;
	// Discard
	case 13:
		return 2.1f;
		break;
	// Draw
	case 14:
		return 1.0f;
		break;
	// Equip
	case 15:
		return 2.1f;
		break;
	// Exhaust
	case 16:
		return 1.0f;
		break;
	// Extend
	case 17:
		return 0.0f;
		break;
	// Force attack
	case 18:
		return 0.0f;
		break;
	// Force block
	case 19:
		return 0.0f;
		break;
	// Freeze
	case 20:
		return 1.0f;
		break;
	// Move
	case 21:
		return 3.1f;
		break;
	// Multiblock
	case 22:
		return 0.0f;
		break;
	// Rearrange
	case 23:
		return 0.0f;
		break;
	// Reduce
	case 24:
		return 3.0f;
		break;
	// Return
	case 25:
		return 1.2f;
		break;
	// Search
	case 26:
		return 3.0f;
		break;
	// Stand
	case 27:
		return 0.1f;
		break;
	// Swap
	case 28:
		return 2.0f;
		break;
	// Token
	case 29:
		return 3.2f;
		break;
	case -1:
		return FindKeyword(_condkey);

		if (Keywordfound == false)
		{
			cout << "CheckEffect: No keyword found.  " << _condkey << endl;
		}

		break;
	}
}

float Card::CheckKeyword(string& _condkey)
{
	int key = -1;

	if (_condkey == "blocker")
	{
		key = as_int(Keywords::Blocker);
	}
	else if (_condkey == "bypass")
	{
		key = as_int(Keywords::Bypass);
	}
	else if (_condkey == "determined")
	{
		key = as_int(Keywords::Determined);
	}
	else if (_condkey == "flying")
	{
		key = as_int(Keywords::Flying);
	}
	else if (_condkey == "fury")
	{
		key = as_int(Keywords::Fury);
	}
	else if (_condkey == "haste")
	{
		key = as_int(Keywords::Haste);
	}
	else if (_condkey == "inanimate")
	{
		key = as_int(Keywords::Inanimate);
	}
	else if (_condkey == "overrun")
	{
		key = as_int(Keywords::Overrun);
	}
	else if (_condkey == "ranged")
	{
		key = as_int(Keywords::Ranged);
	}
	else if (_condkey == "resistance")
	{
		key = as_int(Keywords::Resistance);
	}
	else if (_condkey == "unblockable")
	{
		key = as_int(Keywords::Unblockable);
	}

	switch (key)
	{
		// Blocker
	case 0:
		return 10.0f;
		break;
		// Bypass
	case 1:
		return 20.0f;
		break;
		// Determined
	case 2:
		return 30.0f;
		break;
		// Ranged
	case 3:
		return 40.0f;
		break;
		// Flying
	case 4:
		return 50.0f;
		break;
		// Fury
	case 5:
		return 60.0f;
		break;
		// Haste
	case 6:
		return 70.0f;
		break;
		// Inanimate
	case 7:
		return 80.0f;
		break;
		// Overrun
	case 8:
		return 90.0f;
		break;
		// Resistance
	case 9:
		return 100.0f;
		break;
	case 10:
		//Unblockable
		return 110.0f;
		break;
	case -1:
		cout << "CheckKeyword: No keyword found " << _condkey << endl;
		break;
	}
}

string Card::GetMandArg(string& _condlist)
{
	int foundtarget = -1;
	string arg = "";

	foundtarget = _condlist.find('|');
	for (int i = 0; i < foundtarget; i++)
	{
		arg += _condlist[i];
	}

	_condlist.erase(0, foundtarget + 1);

	arg += "|";

	return arg;
}

string Card::GetOptArg(string& _condlist)
{
	int foundtarget = -1;
	string arg = "";

	foundtarget = _condlist.find(",");

	if (foundtarget != -1 && foundtarget == 0)
	{
		foundtarget = _condlist.find('.');
		if (foundtarget != -1)
		{
			arg += ",";

			for (int i = 1; i < foundtarget; i++)
			{
				arg += _condlist[i];
			}

			_condlist.erase(0, foundtarget + 1);

			arg += ".";
		}
	}

	return arg;
}

string Card::GetArgs(string& _condlist, float _argsize)
{
	string temp = "";
	string arg = "";
	string arglist = "";

	int foundtarget = 0;
	int mandarg = 0;
	int optarg = 0;

	temp = to_string(_argsize);

	foundtarget = temp.find('.');
	for (int i = 0; i < foundtarget; i++)
	{
		arg += temp[i];
	}

	temp.erase(0, foundtarget + 1);

	temp = temp[0];

	mandarg = stoi(arg);
	optarg = stoi(temp);

	for (int i = 0; i < mandarg; i++)
	{
		foundtarget = _condlist.find('|');
		for (int i = 0; i < foundtarget; i++)
		{
			arglist += _condlist[i];
		}

		_condlist.erase(0, foundtarget + 1);

		arglist += "|";
	}

	for (int i = 0; i < optarg; i++)
	{
		foundtarget = _condlist.find(',');
		if (foundtarget != -1 && foundtarget == 0)
		{
			arglist += ",";

			foundtarget = _condlist.find('.');
			for (int i = 1; i < foundtarget; i++)
			{
				arglist += _condlist[i];
			}

			_condlist.erase(0, foundtarget + 1);

			arglist += ".";
		}
	}

	return arglist;
}

string Card::GetEffect(string& _condlist)
{
	int foundtarget = -1;
	string arg = "";

	foundtarget = _condlist.find('~');
	for (int i = 0; i < foundtarget; i++)
	{
		arg += _condlist[i];
	}

	_condlist.erase(0, foundtarget + 1);

	return arg;
}

void Card::CheckFloating(string& _condlist)
{
	string floating = "";
	int founrtarget = -1;

	founrtarget = _condlist.find('#');

	for (int i = 0; i < founrtarget; i++)
	{
		floating += _condlist[i];
	}

	_condlist.erase(0, founrtarget + 1);

	if (floating == "exhaust")
	{
		tapped = true;
	}
	else if (floating == "eot")
	{
		eot = true;
	}
}

void Card::HandleModifers(string& _condlist)
{
	string temp = "";
	string args = "";
	bool exit = false;
	int findarg = -1;
	float num = 0.0f;

	findarg = _condlist.find(',');

	if (findarg != 0)
	{
		exit = true;
	}

	while (exit == false)
	{
		temp = GetOptArg(_condlist);

		modifiers.push_back(temp);

		// If there is a modifier then get it's arg list
		if (temp != "")
		{
			num = CheckModifier(temp);

			if (num == 10.0f)
			{
				string effect = "";

				effect = GetMandArg(_condlist);
				effect = effect.erase(effect.length() - 1);

				num = CheckEffect(effect);

				modifiers.push_back(effect);

				args = GetArgs(_condlist, num);

				if (args == "")
				{
					cout << "HandleModifiers: No effect args found. " << temp << endl;
				}
				else
				{
					modifiers.push_back(args);
				}
			}
			else
			{
				args = GetArgs(_condlist, num);

				if (args == "")
				{
					cout << "HandleModifiers: No args found. " << temp << endl;
				}
				else
				{
					modifiers.push_back(args);
				}
			}
		}

		findarg = _condlist.find(',');

		if (findarg != 0)
		{
			exit = true;
		}
	}
}

void Card::HandleExtraEffect()
{
	string amount = "";
	string replace = "";
	string from = "";
	string to = "";
	string att = "";
	string def = "";
	string gen = "";
	string res = "";
	string time = "";
	string location = "";
	string name = "";
	string abilities = "";
	string keyword = "";
	string lord = "";
	string exc = "";
	string thetarget = "";
	string cost = "";

	int foundtarget = -1;

	if (extraeffect == "add")
	{
		Add(res, amount);
	}
	else if (extraeffect == "ability")
	{
		Ability(thetarget, amount, keyword, exc);
	}
	else if (extraeffect == "boon")
	{
		Boon(att, def, keyword, exc);
	}
	else if (extraeffect == "bounce")
	{
		Bounce(to, amount);
	}
	else if (extraeffect == "cannotattack")
	{
		CannotAttack();
	}
	else if (extraeffect == "cannotattackorblock")
	{
		CannotAttackOrBlock();
	}
	else if (extraeffect == "cannotblock")
	{
		CannotBlock();
	}
	else if (extraeffect == "capture")
	{
		Capture();
	}
	else if (extraeffect == "combatdamage")
	{
		CombatDamage(thetarget);
	}
	else if (extraeffect == "counter")
	{
		Counter();
	}
	else if (extraeffect == "damage")
	{
		amount = GetMandArg(extraeffectargs);
		thetarget = GetOptArg(extraeffectargs);

		Damage(amount, thetarget);
	}
	else if (extraeffect == "destroy")
	{
		Destroy(exc);
	}
	else if (extraeffect == "draw")
	{
		Draw(amount);
	}
	else if (extraeffect == "equip")
	{
		Equip(cost, gen, res);
	}
	else if (extraeffect == "exhaust")
	{
		Exhaust(amount);
	}
	else if (extraeffect == "forceattack")
	{
		ForceAttack();
	}
	else if (extraeffect == "forceblock")
	{
		ForceBlock(thetarget);
	}
	else if (extraeffect == "freeze")
	{
		Freeze(time);
	}
	else if (extraeffect == "move")
	{
		Move(from, to, amount, thetarget);
	}
	else if (extraeffect == "rearrange")
	{
		Rearrange();
	}
	else if (extraeffect == "reduce")
	{
		Reduce(thetarget, amount);
	}
	else if (extraeffect == "return")
	{
		Return(to, replace);
	}
	else if (extraeffect == "search")
	{
		Search(thetarget, from, to);
	}
	else if (extraeffect == "stand")
	{
		Stand(exc);
	}
	else if (extraeffect == "token")
	{
		Token(amount, att, def, name, abilities);
	}
}

void Card::Run(string _condkey, string _modifer, string _modiferlist, string _category, string _target, string _control, string _arglist)
{
	string category = "";
	string target = "";
	string control = "";

	category = _category.erase(_category.length() - 1);
	target = _target.erase(_target.length() - 1);
	control = _control.erase(_control.length() - 1);

	if (target == "perm")
	{
		target = "permanent";
	}
	else if (target == "this")
	{
		target = GetName();
		targetthis = true;
	}
	else if (target == "nonres")
	{
		target = "non resource";
	}

	RunEffect(category, target, control, _condkey, _arglist);
}

void Card::RunEffect(string _category, string _target, string _control, string _condkey, string _arglist)
{
	string modifer = "";
	string category = "";
	string target = "";
	string control = "";
	string amount = "";
	string replace = "";
	string from = "";
	string to = "";
	string att = "";
	string def = "";
	string gen = "";
	string res = "";
	string time = "";
	string location = "";
	string name = "";
	string abilities = "";
	string keyword = "";
	string action = "";
	string lord = "";
	string exc = "";
	string thetarget = "";
	string cost = "";

	string mod = "";
	string args = "";

	int foundtarget = -1;
	int modifiersize = 0;

	category = _category;
	target = _target;
	control = _control;

	modifiersize = modifiers.size();

	// Check modifiers
	for (int i = 0; i < modifiersize; i++)
	{
		mod = modifiers.front();
		modifiers.pop_front();

		if (mod == ",additional.")
		{
			additionalcost = true;

			args = modifiers.front();
			modifiers.pop_front();

			foundtarget = args.find('|');
			for (int i = 0; i < foundtarget; i++)
			{
				additionaltype += args[i];
			}

			args.erase(0, foundtarget + 1);

			foundtarget = args.find('|');
			for (int i = 0; i < foundtarget; i++)
			{
				additionaltarget += args[i];
			}

			args.erase(0, foundtarget + 1);

			foundtarget = args.find('|');
			for (int i = 0; i < foundtarget; i++)
			{
				additionalamount += args[i];
			}

			args.erase(0, foundtarget + 1);
		}
		else if (mod == ",amount.")
		{
			args = modifiers.front();
			modifiers.pop_front();
		}
		else if (mod == ",attack.")
		{

		}
		else if (mod == ",combat damage.")
		{
			thetarget = modifiers.front();
			modifiers.pop_front();

			CombatDamage(category, target, control, thetarget);

			category = "";
		}
		else if (mod == ",dies.")
		{
			string temp = "";

			addextraeffect = true;

			extraeffectcause = mod;
			extraeffect = modifiers.front();
			modifiers.pop_front();
			extraeffectargs = modifiers.front();
			modifiers.pop_front();
		}
		else if (mod == ",enter.")
		{

		}
		else if (mod == ",eot.")
		{
			eot = true;
		}
		else if (mod == ",exhausted.")
		{
			tapped = true;
		}
		else if (mod == ",extend.")
		{
			string gen = "";
			string res = "";
			string args = "";

			extend = true;

			args = modifiers.front();
			modifiers.pop_front();

			foundtarget = args.find('|');
			for (int i = 0; i < foundtarget; i++)
			{
				gen += args[i];
			}

			args.erase(0, foundtarget + 1);

			foundtarget = args.find('.');
			if (foundtarget != -1)
			{
				for (int i = 1; i < foundtarget; i++)
				{
					res += args[i];
				}
			}

			if (res == "")
			{
				res = "0";
			}

			AddCost(costs.GetExtend());

			temptext += "Extend, ";

			AddToAbilityTotalCost(stoi(gen) * 80);
			AddToAbilityTotalCost(stoi(res) * 100);
		}
		else if (mod == ",may attack.")
		{
			canattack = true;
		}
		else if (mod == ",other.")
		{
			other = true;
		}
		else if (mod == ",return a card.")
		{
			amount = modifiers.front();
			modifiers.pop_front();

			Move(category, target, control, "hand", "chronicle", amount, thetarget);
		}
		else if (mod == ",requirement.")
		{
			requirement = true;

			args = modifiers.front();
			modifiers.pop_front();

			foundtarget = args.find('|');
			for (int i = 0; i < foundtarget; i++)
			{
				requirementtype += args[i];
			}

			args.erase(0, foundtarget + 1);

			foundtarget = args.find('.');

			if (foundtarget != -1)
			{
				for (int i = 1; i < foundtarget; i++)
				{
					requirementoption += args[i];
				}

				args.erase(0, foundtarget + 1);
			}
		}

		modifiersize = modifiers.size();
	}

	if (_condkey == "add")
	{
		res = GetMandArg(_arglist);
		amount = GetMandArg(_arglist);

		Add(category, target, control, res, amount);
	}
	else if (_condkey == "ability")
	{
		thetarget = GetMandArg(_arglist);
		amount = GetMandArg(_arglist);
		keyword = GetMandArg(_arglist);
		exc = GetOptArg(_arglist);

		Ability(category, target, control, thetarget, amount, keyword, exc);
	}
	else if (_condkey == "boon")
	{
		att = GetMandArg(_arglist);
		def = GetMandArg(_arglist);
		keyword = GetOptArg(_arglist);
		exc = GetOptArg(_arglist);

		Boon(category, target, control, att, def, keyword, exc);
	}
	else if (_condkey == "bounce")
	{
		to = GetMandArg(_arglist);
		amount = GetMandArg(_arglist);

		Bounce(category, target, control, to, amount);
	}
	else if (_condkey == "cannotattack")
	{
		CannotAttack(category, target, control);
	}
	else if (_condkey == "cannotattackorblock")
	{
		CannotAttackOrBlock(category, target, control);
	}
	else if (_condkey == "cannotblock")
	{
		CannotBlock(category, target, control);
	}
	else if (_condkey == "capture")
	{
		Capture(category, target, control);
	}
	else if (_condkey == "combatdamage")
	{
		thetarget = GetMandArg(_arglist);

		CombatDamage(category, target, control, thetarget);
	}
	else if (_condkey == "counter")
	{
		Counter(category, target, control);
	}
	else if (_condkey == "damage")
	{
		amount = GetMandArg(_arglist);
		thetarget = GetOptArg(_arglist);

		Damage(category, target, control, amount, thetarget);
	}
	else if (_condkey == "destroy")
	{
		exc = GetOptArg(_arglist);

		Destroy(category, target, control, exc);
	}
	else if (_condkey == "discard")
	{
		amount = GetMandArg(_arglist);
		action = GetMandArg(_arglist);
		exc = GetOptArg(_arglist);

		Discard(category, target, control, amount, action, exc);
	}
	else if (_condkey == "draw")
	{
		amount = GetMandArg(_arglist);

		Draw(category, target, control, amount);
	}
	else if (_condkey == "equip")
	{
		cost = GetMandArg(_arglist);
		if (cost == "free|")
		{

		}
		else if (cost == "pay|")
		{
			gen = GetMandArg(_arglist);
			res = GetOptArg(_arglist);
		}

		Equip(category, target, control, cost, gen, res);
	}
	else if (_condkey == "exhaust")
	{
		amount = GetMandArg(_arglist);

		Exhaust(category, target, control, amount);
	}
	else if (_condkey == "forceattack")
	{
		ForceAttack(category, target, control);
	}
	else if (_condkey == "forceblock")
	{
		thetarget = GetMandArg(_arglist);

		ForceBlock(category, target, control, thetarget);
	}
	else if (_condkey == "freeze")
	{
		time = GetMandArg(_arglist);

		Freeze(category, target, control, time);
	}
	else if (_condkey == "move")
	{
		from = GetMandArg(_arglist);
		to = GetMandArg(_arglist);
		amount = GetMandArg(_arglist);
		thetarget = GetOptArg(_arglist);
		
		Move(category, target, control, from, to, amount, thetarget);
	}
	else if (_condkey == "rearrange")
	{
		Rearrange(category, target, control);
	}
	else if (_condkey == "reduce")
	{
		thetarget = GetMandArg(_arglist);
		amount = GetMandArg(_arglist);
		
		Reduce(category, target, control, thetarget, amount);
	}
	else if (_condkey == "return")
	{
		to = GetMandArg(_arglist);
		replace = GetOptArg(_arglist);

		Return(category, target, control, to, replace);
	}
	else if (_condkey == "search")
	{
		thetarget = GetMandArg(_arglist);
		from = GetMandArg(_arglist);
		to = GetMandArg(_arglist);

		Search(category, target, control, thetarget, from, to);
	}
	else if (_condkey == "stand")
	{
		exc = GetOptArg(_arglist);
		
		Stand(category, target, control, exc);
	}
	else if (_condkey == "token")
	{
		amount = GetMandArg(_arglist);
		att = GetMandArg(_arglist);
		def = GetMandArg(_arglist);
		name = GetOptArg(_arglist);
		abilities = GetOptArg(_arglist);

		Token(category, target, control, amount, att, def, name, abilities);
	}

	if (addextraeffect == true)
	{
		HandleExtraEffect();
	}

	if (category == "all")
	{
		MultipleCost(costs.GetAll());
	}
}

void Card::RunWithKeyword(string _category, string _target, string _control, string _keyword)
{
	string category = "";
	string target = "";
	string control = "";
	string keyword = "";

	category = _category.erase(_category.length() - 1);
	target = _target.erase(_target.length() - 1);
	control = _control.erase(_control.length() - 1);
	keyword = _keyword;

	if (category == "all")
	{
		if (control == "you")
		{
			if (type == "Enhancement")
			{
				temptext += "all ";
				temptext += target;
				temptext += "s you control gain ";
				temptext += keyword;
				temptext += " ";
			}
			else
			{
				temptext += "all ";
				temptext += target;
				temptext += "s you control gain ";
				temptext += keyword;
				temptext += " until end of turn ";
			}
		}
	}
	else if (category == "foreach")
	{

	}
	else if (category == "target")
	{
		if (control == "you")
		{
			if (type == "Enhancement")
			{
				temptext += "Enhanced ";
				temptext += target;
				temptext += " you control gains ";
				temptext += keyword;
				temptext += " ";
			}
			else
			{
				temptext += "target ";
				temptext += target;
				temptext += " you control gains ";
				temptext += keyword;
				temptext += " until end of turn ";
			}
		}
		else if (control == "any")
		{
			if (type == "Enhancement")
			{
				temptext += "Enhanced ";
				temptext += target;
				temptext += " gains ";
				temptext += keyword;
				temptext += " ";
			}
			else
			{
				temptext += "target ";
				temptext += target;
				temptext += " gains ";
				temptext += keyword;
				temptext += " until end of turn ";
			}
		}
	}
}

float Card::FindKeyword(string& _condkey)
{
	string key = "";
	float keywordfloat = 0.0f;

	if (type == "Enhancement")
	{
		AddCost(costs.CheckKeyword(_condkey));
	}
	else
	{
		AddCost(costs.CheckKeyword(_condkey));
		SubtractCost(costs.GetUEOT());
	}

	//additional++;

	keywordfloat = CheckKeyword(_condkey);

	Keywordfound = true;

	return keywordfloat;
}
// }

// Function to set which ability line we are editing
void Card::SetText(string _text)
{
	string text = "";

	// Change the ability to all lower case letters
	for (int i = 0; i < _text.length(); i++)
	{
		text += tolower(_text[i]);
	}

	text[0] = toupper(text[0]);

	currentcost = previouscost;

	if (currentcost == CostToEffect::cardcost)
	{
		ability += text;
		ability += " ";
	}
	else if (currentcost == CostToEffect::deathzonecost)
	{
		deathzoneability += text;
		deathzoneability += " ";
	}
}

// Functions for handling the keywords inside the ability or deathzone ability lines
// {
void Card::HandleKeyWord(string _name, string _keyword, int _times)
{
	previouscost = currentcost;

	if (_times != 0)
	{
		keywords += ", ";
	}

	_keyword[0] = toupper(_keyword[0]);

	keywords += _keyword;

	temptext = "";

	SetText(temptext);

	cout << "Card complete: " << _name << endl;
}

void Card::HandleKeyWord(string _name, string _keyword, string _arglist)
{
	int key = -1;
	int foundtarget = 0;
	string condlist = "";

	condlist = _arglist;

	if (_keyword == "activate")
	{
		key = as_int(Commands::activate);
	}
	else if (_keyword == "all")
	{
		key = as_int(Commands::all);
	}
	else if (_keyword == "create")
	{
		key = as_int(Commands::create);
	}
	else if (_keyword == "choose")
	{
		key = as_int(Commands::choose);
	}
	else if (_keyword == "foreach")
	{
		key = as_int(Commands::foreach);
	}
	else if (_keyword == "look")
	{
		key = as_int(Commands::look);
	}
	else if (_keyword == "target")
	{
		key = as_int(Commands::target);
	}
	else if (_keyword == "trigger")
	{
		key = as_int(Commands::trigger);
	}

	switch (key)
	{
	case 0:
		Activate(condlist);
		break;
	case 1:
		All(condlist);
		break;
	case 2:
		Create(condlist);
		break;
	case 3:
		Choose(condlist);
		break;
	case 4:
		ForEach(condlist);
		break;
	case 5:
		Look(condlist);
		break;
	case 6:
		Target(condlist);
		break;
	case 7:
		Trigger(condlist);
		break;
	case -1:
		cout << "HandleKeyword: No keyword found!!  " << _keyword << endl;
	}

	if (currentcost == CostToEffect::deathzonecost)
	{
		AddCost(costs.GetDeathzoneCost());
	}

	SetText(temptext);

	cout << "Card complete: " << _name << endl;
}
// }
