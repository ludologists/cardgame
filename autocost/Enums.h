#pragma once
#include <string>

enum class Commands
{
	activate,
	all,
	create,
	choose,
	foreach,
	look,
	target,
	trigger
};

enum class CostToEffect
{
	cardcost,
	abilitycost,
	deathzonecost
};

enum class Effects
{
	ability,
	add,
	boon,
	bounce,
	cannotattack,
	cannotattackorblock,
	cannotblock,
	capture,
	combatdamage,
	copy,
	counter,
	damage,
	destroy,
	discard,
	draw,
	equip,
	exhaust,
	extend,
	forceattack,
	forceblock,
	freeze,
	move,
	multiblock,
	rearrange,
	reduce,
	retrn,
	search,
	stand,
	swap,
	token
};

enum class Keywords
{
	Blocker,
	Bypass,
	Determined,
	Ranged,
	Flying,
	Fury,
	Haste,
	Inanimate,
	Overrun,
	Resistance,
	Unblockable
};

enum class Modifiers
{
	additional,
	attack,
	combatdamage,
	dies,
	enter,
	extend,
	mayattack,
	other,
	requirement,
	tapped
};

template <typename Enumeration>
auto as_int(Enumeration const value)
-> typename std::underlying_type<Enumeration>::type
{
	return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}