#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "Enums.h"

using namespace std;

class CSVParser
{
private:
	string filename;
	char section[10000];
	char* split;
	string file;
	string* lines;
	string** cols;
	fstream csv;
	int count = 0;
	int colcount = 0;
	char nl = '\n';
	int pos = -1;
public:
	CSVParser();
	~CSVParser();

	// Functions for handling the file
	// {
	void OpenFile(string _filename);
	void ReadFile();
	void CloseFile();
	// }

	// Functions for dividing the file
	// {
	void SplitLines();
	void SplitCols();
	// }

	// Functions for printing specific locations inside the file
	// {
	void PrintLine(int _lineno);
	void PrintCol(int _lineno, int _colno);
	// }

	// Getters for file contents sizes
	// {
	int GetCardCount() { return count - 1; }
	int GetColCount() { return colcount -1; }
	// }

	// Function for getting the text at a specific line and column number
	string GetCol(int _lineno, int _colno);

};