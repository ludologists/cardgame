# Game Plan

- We want to create a game that is fun to watch as well as take part in.

- Make awesome Latex script even more awesome.

- Playtest.

- Make new cards.

- Get an online playtest up and running.

# Instructions

Open the .csv files and input the delimiter as '|' (top left on British keyboards + Alt Gr).

# Compiling Card Images

## Dependencies

For Debian:

> apt install texlive-latex-extra imagemagick

For Arch:

> pacman -S texlive-most imagemagick 

For Void use tlmgr to install the `tcolorbox` LaTeX package.

For Windows:

Install Linux.

